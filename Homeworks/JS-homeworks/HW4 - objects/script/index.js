function createNewUser() {
  firstName = prompt("What is your name?");
  lastName = prompt("what is your last name?");
  let newUser = {
    _firstName: firstName,
    _lastName: lastName,
    set firstName(value) {
      if (typeof value === "string") {
        this._firstName = value;
      }
    },
    set lastName(value) {
      if (typeof value === "string") {
        this._lastName = value;
      }
    },
    get firstName() {
      return this._firstName;
    },
    get lastName() {
      return this._lastName;
    },
    getLogin() {
      return (this._firstName.charAt(0) + this._lastName).toLowerCase();
    },
  };
  console.log(newUser.getLogin());
  return newUser;
}
newUser = createNewUser();
console.log(newUser);
