const buttonElements = document.getElementsByClassName("btn");
let prevButton;
document.addEventListener("keypress", (event) => {
  for (const iterator of buttonElements) {
    if (iterator.dataset.button.toLowerCase() === event.key.toLocaleLowerCase() || event.key === "Enter") {
      if (prevButton) prevButton.classList.remove("active");
      iterator.classList.add("active");
      prevButton = iterator;
      break;
    }
  }
});