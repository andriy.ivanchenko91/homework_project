// Solution through the for which look likes better for me. When you enter too high values for the recursion solution it hangs the browser for quite a lot of time, while loop easily handles this situation even with quite high values. I didn't find a different solution and was forced to learn more about loops and google a lot to get this solution up and running.
//
// n = +prompt("Enter the final number in sequence");
// function doFibonacci(n) {
//   let firstSequence = [0, 1];
//   for (let i = 1; i < n; i++) {
//     firstSequence.push(firstSequence[firstSequence.length - 1] + firstSequence[firstSequence.length - 2]);
//   }
//   return firstSequence[n];
// }
// console.log(doFibonacci(n));
//
// solution through recursion: I don't know why I should use these f0 and f1, I can do it without any
// further variables. Please let me know in case I still need to use them.
n = +prompt("Enter the final number in sequence");
f0 = n - n + 1;
f1 = n - n + 2;
if (n < 0) {
  function doFibonacci(n, f0, f1) {
    if (n >= -1) {
      return n;
    } else {
      return doFibonacci(n + 1) + doFibonacci(n + 2);
    }
  }
}
if (n > 0) {
  function doFibonacci(n, f0, f1) {
    if (n <= 1) {
      return n;
    } else {
      return doFibonacci(n - 1) + doFibonacci(n - 2);
    }
  }
}
console.log(doFibonacci(n));
