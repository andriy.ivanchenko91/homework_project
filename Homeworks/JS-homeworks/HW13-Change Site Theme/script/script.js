    const textContainer = document.querySelector(".theme-description");
    const themeSwitch = document.getElementById("toggleAll");
    const styleSheet = document.getElementById("stylesheet");

    if (localStorage.getItem('siteTheme') === 'light'){
        themeSwitch.setAttribute('checked','checked');
        textContainer.textContent ="Switch to Dark Theme";
    }
    else{
        textContainer.textContent ="Switch to Light Theme";
    }

      if (localStorage.getItem ('siteTheme')==='dark'){
         styleSheet.href = "css/Dark.css";
     }
     else {
         styleSheet.href = "css/Light.css";
     }

themeSwitch.addEventListener("change", (event)=>{
document.querySelector("html").classList.add("visible");
if(themeSwitch.checked){
    styleSheet.href = "css/Light.css";
    textContainer.textContent ="Switch to Dark Theme";
    localStorage.setItem ('siteTheme', 'light');
}
else{
    styleSheet.href = "css/Dark.css";  
    textContainer.textContent ="Switch to Light Theme";
    localStorage.setItem ('siteTheme', 'dark');
}
});

if (localStorage.getItem('siteTheme') === 'light'){
    themeSwitch.setAttribute('checked','checked');
    textContainer.textContent ="Switch to Dark Theme";
}
else{
    textContainer.textContent ="Switch to Light Theme";
}