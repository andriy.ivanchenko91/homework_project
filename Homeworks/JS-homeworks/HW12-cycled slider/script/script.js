const imagesToShow = document.getElementsByClassName("image-to-show");
const buttonStart = document.querySelector(".slide-start");
const buttonStop = document.querySelector(".slide-stop");
const imagesNumber = imagesToShow.length;
const interval = 3000;
let index = 0;
for (const iterator of imagesToShow) {
  iterator.classList.add("zero-opacity");
}
buttonStart.setAttribute("disabled","disabled");
const slideShow = function (imagesNumber) {
  for (const iterator of imagesToShow) {
    iterator.classList.remove("animation")
  }
  imagesToShow[index].classList.add("animation");
  index++;
  if (index === imagesNumber) {
    index = 0;
  }
};
let slideShowTimer = setInterval(() => {
  slideShow(imagesNumber);
}, interval);

buttonStop.addEventListener("click", (event) => {
  clearInterval(slideShowTimer);
  buttonStop.setAttribute("disabled", "disabled")
  buttonStart.removeAttribute("disabled","disabled");
});
buttonStart.addEventListener("click", (event) => {
  
  slideShowTimer = setInterval(() => {
    slideShow(imagesNumber);
  }, interval);
  buttonStart.setAttribute("disabled","disabled");
  buttonStop.removeAttribute("disabled","disabled");
});
