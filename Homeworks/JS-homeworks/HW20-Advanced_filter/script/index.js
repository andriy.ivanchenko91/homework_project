// Реализовать универсальный фильтр массива объектов. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// Технические требования:
// Написать функцию filterCollection(), которая позволит отфильтровать любой массив по заданным ключевым словам.
// Функция должна принимать на вход три основных аргумента:
// массив, который надо отфильтровать
// строку с ключевыми словами, которые надо найти внутри массива (одно слово или несколько слов, разделеных пробелом)
// boolean флаг, который будет говорить, надо ли найти все ключевые слова (true), либо же достаточно совпадения одного из них (false)
// четвертый и последующие аргументы будут являться именами полей, внутри которых надо искать совпадение. Если поле находится не на первом уровне объекта, к нему надо указать полный путь через .. Уровень вложенности полей может быть любой.
let vehicle = {
  name: "Toyota",
  trademarks: {
    camry: {
      description: {
        speed: "veryfast",
        handling: "good",
        space: "medium",
        price: "medium",
      },
      price: 30000,
    },
    markI: {
      description: {
        speed: "fast",
        handling: "bad",
        space: "low",
        price: "veryhigh",
      },
      price: 30000,
    },
    markII: {
      description: {
        speed: "veryfast",
        handling: "medium",
        space: "high",
        price: "medium",
      },
      price: 20000,
    },
    markIII: {
      description: {
        speed: "verYfasT",
        handling: "good",
        space: "good",
        price: "Veryhigh",
      },
      price: 70000,
    },
    Veryold: {
      description: {
        speed: "low",
        handling: "bad",
        space: "low",
        price: "verylow",
      },
      price: 5000,
    },
  },
};
let resultArray = [];
let objectValues = [];
let fields = new Array();
const filterCollection = function (object, keywords, isAllKeyWords) {
  let [, , , ...targetFields] = arguments;
  let keywordarr = keywords.split(" ");
  Object.byString = function (object, string) {
    string = string.replace(/\[(\w+)\]/g, ".$1"); // convert indexes to properties
    string = string.replace(/^\./, ""); // strip a leading dot
    var a = string.split(".");
    for (var i = 0, n = a.length; i < n; ++i) {
      var k = a[i];
      if (k in object) {
        object = object[k];
      } else {
        return;
      }
    }
    return object;
  };
  for (let i = 0; i < targetFields.length; i++) {
    fields[i] = targetFields[i].slice(8);
    objectValues = Object.values(Object.byString(vehicle, fields[i]));
    if (isAllKeyWords) {
      if (
        keywordarr.every((currentValue) => {
          for (const iterator of objectValues) {
            if (iterator.toLowerCase() === currentValue.toLowerCase()) {
              return true;
            }
          }
        })
      ) {
        resultArray[i] = Object.byString(vehicle, fields[i]);
      }
    } else {
      if (
        keywordarr.some((currentValue) => {
          for (const iterator of objectValues) {
            if (iterator.toLowerCase() === currentValue.toLowerCase()) {
              return true;
            }
          }
        })
      ) {
        resultArray[i] += Object.byString(vehicle, fields[i]);
      }
    }
  }
};
filterCollection(vehicle, "veryfast veryslow verygood", true, "vehicle.trademarks.markII.description", "vehicle.trademarks.markIII.description", "vehicle.trademarks.markI.description");
console.log(resultArray);
