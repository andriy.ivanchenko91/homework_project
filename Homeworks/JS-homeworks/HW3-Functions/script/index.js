function checkNum(firstNum, secondNum) {
  return firstNum === "" || secondNum === "" || Number.isNaN(firstNum) || Number.isNaN(+secondNum);
}
function calcNum(firstNum, secondNum, mathOp = "") {
  switch (mathOp) {
    case "+":
      return firstNum + secondNum;
    case "-":
      return firstNum - secondNum;
    case "*":
      return firstNum * secondNum;
    case "/":
      return firstNum / secondNum;
    default:
      console.log("Invalid Math operator");
      break;
  }
}
let firstNum = "";
let secondNum = "";
let mathOp = "";
do {
  if (Number.isNaN(firstNum) || Number.isNaN(secondNum)) {
    firstNum = "Invalid input";
    secondNum = "invalid input";
  }
  firstNum = prompt("Enter the first Number", firstNum);
  firstNum = Number(firstNum);
  mathOp = prompt("Enter the math operator. It could be + - * / ");
  secondNum = prompt("Enter the Second Number", secondNum);
  secondNum = Number(secondNum);
  if (checkNum(firstNum, secondNum)) {
    alert("Invalid numbers, please enter them again");
  }
  if (!(mathOp === "+" || mathOp === "-" || mathOp === "*" || mathOp === "/")) {
    alert("invalid operator");
  }
} while (checkNum(firstNum, secondNum));
console.log(calcNum(firstNum, secondNum, mathOp));
