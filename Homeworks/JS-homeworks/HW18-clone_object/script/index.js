let company1 = {
  name: 1,
  sales: [{ name: "John", salary: 1000 }, { name: "Alice", salary: 600 }, { name: 1 }],
  development: {
    sites: [
      { name: "Peter", salary: 2000 },
      { name: "Alex", salary: 1800 },
    ],
    internals: [{ name: "Jack", salary: 1300 }],
  },
};
let company2 = {};

// function copyObject(targObj) {
// let result = {};
//   for (key in targObj) {
//     if (typeof targObj[key] != "object") {
//       result[key] = targObj[key];
//     } else {
// const innerObject = targObj[key];
//       result[key] = copyObject(innerObject);
//     }
//   }
//   return result;
// }

// company2 = copyObject(company2, company1);

function copyingObject(targObj) {
  if (!targObj || typeof targObj !== "object") return null;

  const destObj = Array.isArray(targObj) ? [] : {};

  for (const key in targObj) {
    if (targObj.hasOwnProperty(key)) {
      if (typeof targObj[key] === "object") {
        destObj[key] = copyingObject(targObj[key]);
      } else {
        destObj[key] = targObj[key];
      }
    }
  }
  return destObj;
}
company2 = copyingObject(company1);
company1.development.sites = { n: 1 };
console.log(company1);
console.log(company2);

// array = [1, 1, 1, 1, 1];

// object = {
//   firstItem: "123",
//   SecondItem: "234",
//   ThirdItem: "567",
//   name: "Alice",
//   salary: 600,
// };

// console.log(array.reduce((prev, current) => prev + current, 0));
// for (let subItem of Object.values(object)) {
//   console.log(subItem);
// }
// let function1 = function (n) {
//   if (n === 10) {
//     console.log("recursion finished");
//     console.log(n);
//     return n;
//   }
//   n++;
//   console.log("1");
//   return function1(n);
// };
// function1(0);
