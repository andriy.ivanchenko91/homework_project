"use strict";
const INCORRECTVALUESPAN = document.createElement("span");
INCORRECTVALUESPAN.textContent = "Please enter correct price!";
const CONTAINER = document.getElementById("container");
const INPUT = document.getElementById("input");
const PRICECONTAINER = document.createElement("div");
PRICECONTAINER.classList.add("priceContainer");
const CLOSEBUTTON = document.createElement("button");
const SPAN = document.createElement("span");
SPAN.style.display = "inline-block";
CLOSEBUTTON.style.display = "inline-block";
PRICECONTAINER.append(SPAN);
PRICECONTAINER.append(CLOSEBUTTON);
CLOSEBUTTON.textContent = "X";
INPUT.addEventListener("focus", (event) => {
  event.target.style.border = "3px solid green";
});
INPUT.addEventListener("blur", (event) => {
  event.target.style.border = "";
  if (!(event.target.value >= 0 || Number.isInteger(event.target.value))) {
    event.target.style.border = "2px solid red";
    PRICECONTAINER.remove();
    CONTAINER.append(INCORRECTVALUESPAN);
  } else if (event.target.value) {
    INCORRECTVALUESPAN.remove();
    SPAN.textContent = `Текущая цена: ${event.target.value}`;
    CONTAINER.prepend(PRICECONTAINER);
    event.target.style.color = "green";
  }
});
CLOSEBUTTON.addEventListener("click", (event) => {
  PRICECONTAINER.remove();
});
