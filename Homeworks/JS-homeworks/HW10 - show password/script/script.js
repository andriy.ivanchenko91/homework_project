const passwordForm = document.querySelector(".password-form");
const submitButton = document.querySelector(".btn");
const inputForm = document.querySelectorAll(".password-input");
const warningText = document.createElement("span");
let passwords = [];
warningText.textContent = "Password do not match, try again!";
passwordForm.addEventListener("click", (event) => {
  let target = event.target;
  if (target.tagName == "BUTTON") {
    event.preventDefault();
    document.querySelectorAll(".password-input").forEach((element) => {
      passwords.push(element.value);
    });
    console.log (passwords[0]);
    if(passwords[0]!=="" &&passwords[1]!==""){
    if (passwords[0] === passwords[1]){
      alert("You are welcome!");
    } else{
      passwordForm.append(warningText);
    }
  }
  else {
    alert ("Password cannot be empty!");
  }
    passwords = [];
  }
  if (target.tagName !== "I") return;
  togglevisibility(target);
});
let togglevisibility = function (targetItem) {
  if (targetItem.classList.contains("fa-eye-slash")) {
    targetItem.parentNode.querySelector(".password-input").type = "text";
  } else {
    targetItem.parentNode.querySelector(".password-input").type = "password";
  }
  targetItem.classList.toggle("fa-eye-slash");
  targetItem.classList.toggle("fa-eye");
};