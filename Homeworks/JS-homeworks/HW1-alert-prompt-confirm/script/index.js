let usersAge;
let userName;
const MIN_AGE = 18;
const MAX_AGE = 22;
let prevAge;
let prevName;
do {
  userName = prompt("What is your name?", prevName);
  prevName = userName;
  usersAge = +prompt("Enter your age", prevAge);
  prevAge = usersAge;
  if (Number.isNaN(usersAge)) {
    prevAge = "Last entered value is not a number!";
  }
  if (userName === "" || userName === null) {
    alert("Sorry, name is incorrect");
  }
  if (!Number.isInteger(usersAge) || usersAge == 0) {
    alert("Please enter the correct age");
  }
} while (userName === "" || !Number.isInteger(usersAge) || userName === null);

if (usersAge < MIN_AGE) {
  alert("You are not allowed to visit this site");
} else if (usersAge >= 18 && usersAge <= 22) {
  if (confirm("Are you sure?") === true || usersAge > MAX_AGE) {
    alert("Welcome, " + userName);
  } else {
    alert("You are not allowed to visit this site");
  }
}
console.log(confirm);
