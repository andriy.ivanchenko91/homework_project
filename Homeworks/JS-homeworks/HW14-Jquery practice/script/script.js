$(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 600, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});

const btn = document.querySelector(".scroll-to-top-button");
const hidebutton = document.querySelector(".hide-button");
const hotNewsSection = document.querySelector(".hot-news");

hidebutton.addEventListener("click", () => {
$(hotNewsSection).slideToggle("500");
});
console.log (btn);
btn.addEventListener ("click", (event) => {
    window.scrollTo(0,0);
});
