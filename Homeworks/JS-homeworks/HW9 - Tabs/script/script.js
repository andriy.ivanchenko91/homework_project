let selectedTab;
const tabsParent = document.querySelector(".tabs");
const textContent = document.getElementsByClassName("content");

let prevElement;
tabsParent.addEventListener("click", (event) => {
  highlightItem(event.target);
});
const highlightItem = function (target) {
  if (selectedTab) {
    selectedTab.classList.remove("active");
    prevElement !== undefined ? prevElement.classList.add("invisibleContent") : console.log("No such content element");
  }
  selectedTab = target;
  selectedTab.classList.add("active");
  for (let i = 0; i < textContent.length; i++) {
    if (selectedTab.textContent === textContent[i].dataset.itemname) {
      textContent[i].classList.remove("invisibleContent");
      prevElement = textContent[i];
    }
  }
};
