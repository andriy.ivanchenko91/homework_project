function createNewUser() {
  let firstName;
  let lastName;
  let dateOfBirth;
  do {
    firstName = prompt("What is your name?");
    lastName = prompt("what is your last name?");
    dateOfBirth = prompt("What is your age? Please enter it in the following format: dd.mm.yyyy. Other formats are not accepted.");
    if (!/^\d{2}\.\d{2}\.\d{4}$/.test(dateOfBirth)) {
      alert("Please enter the correct date of birth dd.mm.yyyy");
    }
  } while (!/^\d{2}\.\d{2}\.\d{4}$/.test(dateOfBirth));
  const newUser = {
    birthday: dateOfBirth,
    _firstName: firstName,
    _lastName: lastName,
    set firstName(value) {
      if (typeof value === "string") {
        this._firstName = value;
      }
    },
    set lastName(value) {
      if (typeof value === "string") {
        this._lastName = value;
      }
    },
    get firstName() {
      return this._firstName;
    },
    get lastName() {
      return this._lastName;
    },
    getLogin() {
      return (this._firstName.charAt(0) + this._lastName).toLowerCase();
    },
    getAge(dateOfBirth) {
      dateOfBirthArray = dateOfBirth.split(".");
      normalizedDate = Date.parse(dateOfBirthArray[1] + "-" + dateOfBirthArray[0] + "-" + dateOfBirthArray[2]);
      let currentDate = Date.parse(new Date());
      console.log("Entered year - " + dateOfBirth);
      console.log("Milliseconds since the year - " + normalizedDate);
      calculatedAge = currentDate - normalizedDate;
      calculatedAge = Math.floor(calculatedAge / 31536000000);
      return calculatedAge;
    },
    getPassword() {
      dateOfBirthArray = dateOfBirth.split(".");
      return this.firstName.charAt(0) + this.lastName.toLowerCase() + dateOfBirthArray[2];
    },
  };
  console.log(newUser.getLogin());
  console.log(newUser.getAge(dateOfBirth));
  console.log(newUser.getPassword(firstName, lastName, this.yearOfBirth));
  return newUser;
}
newUser = createNewUser();
console.log(newUser);
