const addMoreCards = function () {
  const ourAmazingWorkContent = document.querySelector(".amazing-works-photo-grid");
  const documentFrag = document.createDocumentFragment();
  dataSetElement = "";
  for (let i = 0; i <= 16; i++) {
    const additionalCardContainer = document.createElement("div");
    if (i <= 4) {
      dataSetElement = "webDesign";
    } else if (i > 4 && i <= 8) {
      dataSetElement = "graphicDesign";
    } else if (i > 8 && i <= 12) {
      dataSetElement = "landingPages";
    } else if (i > 12 && i <= 16) {
      dataSetElement = "wordPress";
    }

    additionalCardContainer.dataset.itemName = dataSetElement;
    additionalCardContainer.classList.add("amazing-works-item");
    const elementTemplate = `
            <div class="amazing-works-inner-card">
              <div class="amazing-works-card-front" style="background-image: url(./img/Amazing-work-additional/${i}.jpg)"></div>
              <div class="back-side-card">
                <div class="back-side-card-icon-container">
                  <div class="back-side-icons">
                    <svg
                      aria-hidden="true"
                      focusable="false"
                      data-prefix="fas"
                      data-icon="link"
                      class="svg-inline--fa fa-link fa-w-16 link-icon"
                      role="img"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 512 512"
                    >
                      <path
                        d="M326.612 185.391c59.747 59.809 58.927 155.698.36 214.59-.11.12-.24.25-.36.37l-67.2 67.2c-59.27 59.27-155.699 59.262-214.96 0-59.27-59.26-59.27-155.7 0-214.96l37.106-37.106c9.84-9.84 26.786-3.3 27.294 10.606.648 17.722 3.826 35.527 9.69 52.721 1.986 5.822.567 12.262-3.783 16.612l-13.087 13.087c-28.026 28.026-28.905 73.66-1.155 101.96 28.024 28.579 74.086 28.749 102.325.51l67.2-67.19c28.191-28.191 28.073-73.757 0-101.83-3.701-3.694-7.429-6.564-10.341-8.569a16.037 16.037 0 0 1-6.947-12.606c-.396-10.567 3.348-21.456 11.698-29.806l21.054-21.055c5.521-5.521 14.182-6.199 20.584-1.731a152.482 152.482 0 0 1 20.522 17.197zM467.547 44.449c-59.261-59.262-155.69-59.27-214.96 0l-67.2 67.2c-.12.12-.25.25-.36.37-58.566 58.892-59.387 154.781.36 214.59a152.454 152.454 0 0 0 20.521 17.196c6.402 4.468 15.064 3.789 20.584-1.731l21.054-21.055c8.35-8.35 12.094-19.239 11.698-29.806a16.037 16.037 0 0 0-6.947-12.606c-2.912-2.005-6.64-4.875-10.341-8.569-28.073-28.073-28.191-73.639 0-101.83l67.2-67.19c28.239-28.239 74.3-28.069 102.325.51 27.75 28.3 26.872 73.934-1.155 101.96l-13.087 13.087c-4.35 4.35-5.769 10.79-3.783 16.612 5.864 17.194 9.042 34.999 9.69 52.721.509 13.906 17.454 20.446 27.294 10.606l37.106-37.106c59.271-59.259 59.271-155.699.001-214.959z"
                      ></path>
                    </svg>
                  </div>
                  <div class="back-side-icons">
                    <div class="back-side-icon-rectangle-shape"></div>
                  </div>
                </div>
                <p class="back-side-card-heading">creative design</p>
                <p class="back-side-card-subheading">Web Design</p>
              </div>
            </div>
          </div>`;
    additionalCardContainer.innerHTML = elementTemplate;
    documentFrag.append(additionalCardContainer);
  }
  ourAmazingWorkContent.append(documentFrag);
};
const removeAllActiveElements = function (tabChildrenElements, contentElements) {
  for (let i = 0; i < tabChildrenElements.children.length; i++) {
    if (tabChildrenElements.classList.contains("our-services-list")) {
      tabChildrenElements.children[i].classList.remove("our-services-tab-active");
      tabChildrenElements.children[i].classList.add("our-services-tab");
    } else {
      tabChildrenElements.children[i].classList.remove("our-amazing-work-tab-active");
      tabChildrenElements.children[i].classList.add("our-amazing-work-tab");
    }
  }
  for (let i = 0; i < contentElements.children.length; i++) {
    contentElements.children[i].classList.add("mode-inactive");
  }
};
const filterContent = function (contentToBeFiltered, target) {
  for (let i = 0; i < contentToBeFiltered.children.length; i++) {
    if (contentToBeFiltered.children[i].dataset.itemName === target.dataset.elementId) {
      contentToBeFiltered.children[i].classList.remove("mode-inactive");
    } else if (target.dataset.elementId === "all") {
      contentToBeFiltered.children[i].classList.remove("mode-inactive");
    }
  }
};
const addSelectedContent = function (selectedContent) {};
document.querySelector(".our-services-list").addEventListener("click", (event) => {
  ourServicesTabsSwitching(event.target);
});
const ourServicesTabsSwitching = function (target) {
  if (target.tagName === "UL") {
    return;
  }
  const ourServicesTabs = document.querySelector(".our-services-list");
  const ourServicesContent = document.querySelector(".our-services-content-container");
  removeAllActiveElements(ourServicesTabs, ourServicesContent);
  target.classList.add("our-services-tab-active");
  target.classList.remove("our-services-tab");
  filterContent(ourServicesContent, target);
};
document.querySelector(".load-more-button").addEventListener("click", (event) => {
  document.querySelector(".load-more-button-container").remove();
  document.querySelector(".loading-spinner-container").classList.remove("mode-inactive");
  document.querySelector(".loading-spinner").classList.remove("mode-inactive");
  setTimeout(() => {
    document.querySelector(".loading-spinner").classList.add("mode-inactive");
    addMoreCards();
  }, 1000);
});
document.querySelector(".our-amazing-work-tabs").addEventListener("click", (event) => {
  ourAmazingWorkTabsSwitching(event.target);
});
const ourAmazingWorkTabsSwitching = function (target) {
  if (target.tagName === "UL") {
    return;
  }
  const ourAmazingWorkTabs = document.querySelector(".our-amazing-work-tabs");
  const ourAmazingWorkContent = document.querySelector(".amazing-works-photo-grid");
  removeAllActiveElements(ourAmazingWorkTabs, ourAmazingWorkContent);
  target.classList.add("our-amazing-work-tab-active");
  target.classList.remove("our-amazing-work-tab");
  filterContent(ourAmazingWorkContent, target);
};

$(".slider-for").slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: ".slider-nav",
});
$(".slider-nav").slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  asNavFor: ".slider-for",
  prevArrow: $(".customers-tabs-leftarrow"),
  nextArrow: $(".customers-tabs-rightarrow"),
  centerMode: true,
  centerPadding: "0px",
  draggable: false,
  focusOnSelect: true,
});
$(".slider-description").slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
});
const slickObject = $(".slider-nav").slick("getSlick");
console.log(slickObject);
const currentSlideIndexInit = $(".slider-nav").slick("slickCurrentSlide");
$(slickObject.$slides.get(currentSlideIndexInit))[0].classList.add("flex-up-slick-slider");
const currentSlideIndex = $(".slider-nav").slick("slickCurrentSlide");
$(".slider-description").slick("slickGoTo", `${currentSlideIndex}`);
$(".slider-nav").on("afterChange", function (event, slick, direction) {
  const currentSlideIndex = $(".slider-nav").slick("slickCurrentSlide");
  $(".slider-description").slick("slickGoTo", `${currentSlideIndex}`);
});
let previousElement = document.querySelector(".slick-center");
let parentElementSlider;
$(".slider-nav").on("beforeChange", function (event, slick, currentSlide, nextSlide) {
  const currentSlideDom = $(slick.$slides.get(currentSlide));
  console.log(currentSlideDom);
  currentSlideDom[0].classList.remove("flex-up-slick-slider");
});
$(".slider-nav").on("afterChange", function (event, slick, currentSlide) {
  const currentSlideDom = $(slick.$slides.get(currentSlide));
  currentSlideDom[0].classList.add("flex-up-slick-slider");
});
document.querySelector(".slider-nav").querySelector(".slick-track").classList.add("slick-track-flex");
