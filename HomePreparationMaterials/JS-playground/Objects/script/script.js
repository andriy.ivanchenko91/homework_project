$(".slider-for").slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: ".slider-nav",
});
$(".slider-nav").slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  infitie: true,
  asNavFor: ".slider-for",
});
$(".slider-description").slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
});

$(".slider-nav").on("afterChange", function (event, slick, direction) {
  $(".slider-description").slick("next");
});
